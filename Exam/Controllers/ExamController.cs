﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Exam.Controllers
{
    public class ExamController : Controller
    {
        ExamEntities1 examDB = new ExamEntities1();
        // GET: Exam
        public ActionResult Index()
        {
            return View(examDB.Exams.ToList());
        }

        // GET: Exam/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        //create exam
        // GET: Exam/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Exam/Create
        [HttpPost]
        public ActionResult Create(Exam exam)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    examDB.Exams.Add(exam);
                    examDB.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(exam);
            }
            catch
            {
                return View();
            }
        }

        // GET: Exam/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Exam/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Exam/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Exam/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
